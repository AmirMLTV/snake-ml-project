from socket import timeout
import numpy as np
import copy
import random
import time
import os
import pygame

def update_weights(weights, score, prev_score):
    alpha = 0.001
    sum_s = sum_score(score)
    delta = sum_s - prev_score
    weights[0] = weights[0] + (alpha * delta * score[0])
    weights[1] = weights[1] + (alpha * delta * score[1])
    weights[2] = weights[2] + (alpha * delta * score[2])
    weights[3] = weights[3] + (alpha * delta * score[3])
    weights[4] = weights[4] + (alpha * delta * score[4])
    weights[5] = weights[5] + (alpha * delta * score[5])
#     weights[6] = weights[6] + (alpha * delta * score[6])
    
    return weights
    
def cal_scores(board, snake, prev_food):
    # snake head distance from food
    f1 = dis_direct_from_food(board, snake)
    # snake head diagonal distance with food
    f2 = dis_diag_from_food(board, snake)
    # snake head distance from obstacles and wall
#     f3 = dis_direct_from_obst(board, snake)
#     # snake head diagonal distance from obstacles and wall
#     f4 = dis_diag_from_obst(board, snake)
    # snake head distance from food (steps to food)
    f5 = dis_from_food(board, snake, prev_food)
    # snake head distance from its body
    f6 = dis_from_self(board, snake)
    # if it was eaten
    f7 = eat_food(board, snake, prev_food)
    return [1, f1, f2, f5, f6, f7]
    
def dis_direct_from_food(board, snake):
    #direct distance from food
    f = 1
    filled = get_filled_cells(board)
    snake_head = get_snake_head(snake)
    x, y = snake_head
    for i in filled:
        if(board[i[0]][i[1]] == 3):
            food_loc = i
    max_x = board.shape[0]
    max_y = board.shape[1]
    for i in range(1,4):
        if(board[(x-i) % max_x][(y)% max_y] == 3 and (x-i) > 0):
            f = f + (999/i)
            return f
        if(board[(x+i) % max_x][(y)% max_y] == 3 and (x+i) < max_x):
            f = f + (999/i)
            return f
        if(board[(x) % max_x][(y-i)% max_y] == 3 and (y-i) > 0):
            f = f + (999/i)
            return f
        if(board[(x) % max_x][(y+i)% max_y] == 3 and (y+i)<max_y):
            f = f + (999/i)
            return f
    return f

def dis_diag_from_food(board, snake):
    # diagonal distance 1 block
    f = 1
    filled = get_filled_cells(board)
    snake_head = get_snake_head(snake)
    x, y = snake_head
    for i in filled:
        if(board[i[0]][i[1]] == 3):
            food_loc = i
    max_x = board.shape[0]
    max_y = board.shape[1]
    for i in range(1,7):
        if(board[(x-i) % max_x][(y-i)% max_y] == 3  and (x-i) > 0 and (y-i) > 0):
            f = f + (900/i)
            return f
        if(board[(x-i) % max_x][(y+i)% max_y] == 3  and (x-i) > 0 and (y+i) < max_y):
            f = f + (900/i)
            return f
        if(board[(x+i) % max_x][(y-i)% max_y] == 3 and (x+i) < max_x and (y-i) > 0):
            f = f + (900/i)
            return f
        if(board[(x+i) % max_x][(y+i)% max_y] == 3 and (x+i) < max_x and (y+i) < max_y):
            f = f + (900/i)
            return f
    return f

def dis_direct_from_obst(board, snake):
    #direct distance from food
    filled = get_filled_cells(board)
    snake_head = get_snake_head(snake)
    x, y = snake_head
    f = 1
    
    max_x = board.shape[0]
    max_y = board.shape[1]
    for i in range(1,7):
        if((board[(x-i) % max_x][(y)% max_y] == 2 or board[(x-i) % max_x][(y)% max_y] == 9) and (x-i) > 0):
            f = f + (i*50*i)
            return f
        if((board[(x+i) % max_x][(y)% max_y] == 2 or board[(x+i) % max_x][(y)% max_y] == 9) and (x+i) < max_x):
            f = f + (i*50*i)
            return f
        if((board[(x) % max_x][(y-i)% max_y] == 2 or board[(x) % max_x][(y-i)% max_y] == 9) and (y-i) > 0):
            f = f + (i*50*i)
            return f
        if((board[(x) % max_x][(y+i)% max_y] == 2 or board[(x) % max_x][(y+i)% max_y] == 9)and (y+i)<max_y):
            f = f + (i*50*i)
            return f
    return f

def dis_diag_from_obst(board, snake):
    # diagonal distance 1 block
    f = 1
    filled = get_filled_cells(board)
    snake_head = get_snake_head(snake)
    x, y = snake_head
    for i in filled:
        if(board[i[0]][i[1]] == 3):
            food_loc = i
    max_x = board.shape[0]
    max_y = board.shape[1]
    for i in range(1,4):
        if(board[(x-i) % max_x][(y-i)% max_y] == 2 or board[(x-i) % max_x][(y-i)% max_y] == 9):
            f = f + (i*30*i)
            return f
        elif(board[(x-i) % max_x][(y+i)% max_y] == 2 or board[(x-i) % max_x][(y-i)% max_y] == 9):
            f = f + (i*30*i)
            return f
        elif(board[(x+i) % max_x][(y-i)% max_y] == 2 or board[(x-i) % max_x][(y-i)% max_y] == 9):
            f = f + (i*30*i)
            return f
        elif(board[(x+i) % max_x][(y+i)% max_y] == 2 or board[(x-i) % max_x][(y-i)% max_y] == 9):
            f = f + (i*30*i)
            return f
    return f

def dis_from_food(board, snake, prev_food):
    filled = get_filled_cells(board)
    snake_head = get_snake_head(snake)
    food_loc = prev_food
#     print(food_loc)
    for i in filled:
        if(board[i[0]][i[1]] == 3):
            food_loc = i
#             print(i)
    dis_food = 1
    if snake_head[0] >= food_loc[0]:
        if snake_head[1] >= food_loc[1]:
            dis_food = 900/ (((snake_head[0] - food_loc[0]) + (snake_head[1] - food_loc[1]))+1)
            return dis_food
        elif snake_head[1] < food_loc[1]:
            dis_food = 900/ (((snake_head[0] - food_loc[0]) + (food_loc[1] - snake_head[1]))+1)
            return dis_food
    elif snake_head[0] < food_loc[0]:
        if snake_head[1] >= food_loc[1]:
            dis_food = 900/ (((food_loc[0] - snake_head[0]) + (snake_head[1] - food_loc[1]))+1)
            return dis_food
        elif snake_head[1] < food_loc[1]:
            dis_food = 900/ (((food_loc[0] - snake_head[0]) + (food_loc[1] - snake_head[1]))+1)
            return dis_food

def dis_from_self(board, snake):
    filled = get_filled_cells(board)
    snake_head = get_snake_head(snake)
    x, y = snake_head
    snake_body = snake[-2]
    f = 801
    max_x = board.shape[0]
    max_y = board.shape[1]
    
    if(board[(x-1) % max_x][(y)% max_y] == 1 and x-1 != snake_body[0] and y != snake_body[1]):
        f = f - 800
    elif(board[(x+1) % max_x][(y)% max_y] == 1 and x+1 != snake_body[0] and y != snake_body[1]):
        f = f - 800
    elif(board[(x) % max_x][(y-1)% max_y] == 1 and x != snake_body[0] and y-1 != snake_body[1]):
        f = f - 800
    elif(board[(x) % max_x][(y+1)% max_y] == 1 and x != snake_body[0] and y+1 != snake_body[1]):
        f = f - 800
    
    return f
def eat_food(board, snake, prev_food):
    snake_head = get_snake_head(snake)
    f = 1
    if(snake_head[0] == prev_food[0] and snake_head[1] == prev_food[1]):
        f = 10000
    return f

def sum_score(score):
    sum = 0
    for i in score:
        sum = sum + i
        
    return sum
# def dis_from_obst(board, snake):
#     snake_head = get_snake_head(snake)
#     filled = get_filled_cells(board)
#     obstacles = []
#     for i in filled:
#         if(board[i[0]][i[1]] == 2):
#             obstacles.append(i)

#     obst1, obst2, obst3, obst4 = obstacles[0:4], obstacles[4:8], obstacles[8:12], obstacles[12:16]
#     obsts = [obst1, obst2, obst3, obst4]
#     check = [True,True,True,True]
#     dis = [0,0,0,0]
#     for i in range(4):
#         if(snake_head[0] >= obsts[i][2][0] and check[i]):
#             if(snake_head[1] >= obsts[i][3][1] and check[i]):
#                     dis[i] = (snake_head[0] - obsts[i][3][0]) + (snake_head[1] - obsts[i][3][1])
#                     check[i] = False
#             if(snake_head[1] <= obsts[i][2][1] and check[i]):
#                     dis[i] = (snake_head[0] - obsts[i][3][0]) + (obsts[i][2][1] - snake_head[1])
#                     check[i] = False
        
#         if(snake_head[0] <= obsts[i][0][0] and check[i]):
#             if(snake_head[1] >= obsts[i][3][1] and check[i]):
#                     dis[i] = (obsts[i][3][0] - snake_head[0]) + (snake_head[1] - obsts[i][3][1])
#                     check[i] = False
#             if(snake_head[1] <= obsts[i][2][1] and check[i]):
#                     dis[i] = (obsts[i][3][0] - snake_head[0]) + (obsts[i][2][1] - snake_head[1])
#                     check[i] = False
        
#     return dis[0],dis[1],dis[2],dis[3]

# def dis_from_wall(board, snake):
#     snake_head = get_snake_head(snake)
    
#     # distance from right wall
#     f_r = board.shape[0] - (snake_head[1] + 1)
    
#     # distance from left wall
#     f_l = snake_head[1]
    
#     # distance from top wall
#     f_t = snake_head[0]
    
#     # distance from down wall
#     f_d = board.shape[0] - (snake_head[0] + 1)

#     return f_r, f_l, f_t, f_d


def get_random_rect_from_np_array(array,x,y):
    # random number
    rand_x = np.random.randint(0,array.shape[0])
    rand_y = np.random.randint(0,array.shape[1])

    max_x = array.shape[0]
    max_y = array.shape[1]

    # empty array
    result = []

    # binary random
    rand_binary = np.random.randint(0,2)  # 0 for vertical , 1 for horizontal
    if(rand_binary == 0):
        for i in range (x):
            for j in range(y):
                result.append([(rand_x+i) % max_x,(rand_y+j) % max_y])
    elif(rand_binary == 1):
        for i in range (x):
            for j in range(y):
                result.append([(rand_x+j) % max_x,(rand_y+i) % max_y])

    return result

def get_updated_board(board , x , y , exclude_list,character):
    while True:
        area = get_random_rect_from_np_array(board,x,y)
        # if elements of area are not in exclude_list
        if(not(any(elem in area for elem in exclude_list))):
            for i in area:
                board[i[0]][i[1]] = character
            return board

def get_filled_cells(board):
    filled_cells = []
    for i in range(board.shape[0]):
        for j in range(board.shape[1]):
            if(board[i][j] != 0):
                filled_cells.append([i,j])
    return filled_cells

def get_snake_head(snake):
    return snake[-1]

def create_board():

    # 0 for empty
    board = np.zeros((20, 20))
    board.fill(0)

    # -1 for walls filled with 9
    board[0,:] = 9
    board[-1,:] = 9
    board[:,0] = 9
    board[:,-1] = 9
    


    # 1 for snake
    filled = get_filled_cells(board)
    board = get_updated_board(board , 3 , 1 , filled , 1 )
    filled = get_filled_cells(board)
    
    
    # get snake location
    snake = []
    for i in filled:
        if(board[i[0]][i[1]] == 1):
            snake.append(i)
    
    
    # 2 for obstacle
    board = get_updated_board(board , 2 , 2 , filled , 2 )
    filled = get_filled_cells(board)
    board = get_updated_board(board , 2 , 2 , filled , 2 )
    filled = get_filled_cells(board)
    board = get_updated_board(board , 2 , 2 , filled , 2 )
    filled = get_filled_cells(board)
    board = get_updated_board(board , 2 , 2 , filled , 2 )
    filled = get_filled_cells(board)
    
    
    # 3 for food
    board = get_updated_board(board , 1 , 1 , filled , 3 )
    filled = get_filled_cells(board)
    
    
    return board, snake

def available_moves(board,x,y):
    moves = []
    max_x = board.shape[0]
    max_y = board.shape[1]
    if(board[(x-1) % max_x][y] == 0 or board[(x-1) % max_x][y] == 3):
        moves.append([(x-1) % max_x,y])
    if(board[(x+1) % max_x][y] == 0 or board[(x+1) % max_x][y] == 3):
        moves.append([(x+1)%max_x,y])
    if(board[x][(y-1)%max_y] == 0 or board[x][(y-1)%max_y] == 3):
        moves.append([x,(y-1)%max_y])
    if(board[x][(y+1)%max_y] == 0 or board[x][(y+1)%max_y] == 3):
        moves.append([x,(y+1)%max_y])
    return moves

def is_move_possible(board , snake_head , move):
    if move not in available_moves(board,snake_head[0],snake_head[1]):
        return (False,"obstacle")
    food = get_food_location(board)
    if(move == food):
        return (True,"food")
    return (True,'empty')

def update_snake(snake,move,reason):
    if reason == 'food':
        snake.append(move)
    elif reason == 'empty':
        snake.append(move)
        snake.pop(0)
    return (snake,move)

def get_food_location(board):
    for i in range(board.shape[0]):
        for j in range(board.shape[1]):
            if(board[i][j] == 3):
                return [i,j]

def update_board_after_move(board , snake , move , reason):
    for i in range(board.shape[0]):
        for j in range(board.shape[1]):
            if(board[i][j] == 1):
                board[i][j]=0
    
    for i in snake:
        board[i[0]][i[1]] = 1

    if(reason == 'food'):
        board[move[0]][move[1]] = 0

    return board

def move_snake(board , snake , snake_head , move):
    board,snake,snake_head,move = copy.deepcopy(board),copy.deepcopy(snake),copy.deepcopy(snake_head),copy.deepcopy(move)
    # check if move is possible
    possible,reason = is_move_possible(board , snake_head , move)

    if(possible and reason == "food"):
        snake , snake_head = update_snake(snake ,  move , 'food')
        board = update_board_after_move(board , snake , move , 'food')
        filled = get_filled_cells(board)
        board = get_updated_board(board , 1 , 1 , filled , 3 )
        food = get_food_location(board)
        if(food == None):
            print('no food why ? ')
        return (board,snake,snake_head , True) 
    elif(possible and reason == "empty"):
        snake , snake_head = update_snake(snake ,  move , 'empty')
        board = update_board_after_move(board , snake , move , 'empty')
        return (board,snake,snake_head ,True) 
    elif(move in snake):
        return (board,snake,snake_head,False) 
    else:
        # invalid move
        return (board,snake,snake_head,False) 

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255,0,0)
GREEN = (0,255,0)
BLUE = (0,0,255)
WINDOW_HEIGHT = 400
WINDOW_WIDTH = 400
GOLD = (255,215,0)

def drawGrid(board):
    blockSize = 20 #Set the size of the grid block

    for x in range(0, WINDOW_WIDTH, blockSize):
        for y in range(0, WINDOW_HEIGHT, blockSize):
            rect = pygame.Rect(x, y, blockSize, blockSize)
            if(board[int(x/20)][int(y/20)]==9):
                pygame.draw.rect(SCREEN, RED, rect, 10)
            elif(board[int(x/20)][int(y/20)]==1):
                pygame.draw.rect(SCREEN, GREEN, rect, 10)
            elif(board[int(x/20)][int(y/20)]==2):
                pygame.draw.rect(SCREEN, BLUE, rect, 10)
            elif(board[int(x/20)][int(y/20)]==3):
                pygame.draw.rect(SCREEN, GOLD, rect, 10)
            else:
                pygame.draw.rect(SCREEN, WHITE, rect, 1)

def show_board(board):
    global SCREEN, CLOCK
    pygame.init()
    SCREEN = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
    CLOCK = pygame.time.Clock()
    SCREEN.fill(BLACK)
    drawGrid(board)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()

    pygame.display.update()



def check_if_game_over(board,snake):
    return True

def print_board(board):
    for i in range(board.shape[0]):
        print('\n')
        for j in range(board.shape[1]):
            print(board[i][j],end=" ")

def reset_screen():
    if os.name == 'nt':
        os.system('cls')
    else:
        os.system('clear')


def main():
    
#     print(board)
#     print(snake)
    
    weights = [0, 0, 0, 0, 0, 0]
    n = 100
    while(n > 0):
        board, snake = create_board()
        while True:
            show_board(board)
            # try:
            # wait 0.2 seconds
            # reset_screen
            # time.sleep(2.0)
            # print_board(board)
            snake_head = get_snake_head(snake)
            moves = available_moves(board,snake_head[0],snake_head[1])

            # random int from 0 to len(moves)
            #rand_int = random.randint(0, len(moves)-1)
    #             board , snake , snake_head = move_snake(board,snake,snake_head,moves[rand_int])
            prev_board = copy.deepcopy(board)
            prev_food = get_food_location(prev_board)
            if(prev_food == None):
                print('none')
                # TODO add food to board
                filled = get_filled_cells(prev_board)
                prev_board = get_updated_board(prev_board , 1 , 1 , filled , 3 )
                board = prev_board
                prev_food = get_food_location(prev_board)

            prev_score = cal_scores(board, snake, prev_food)
            prev_sum = sum_score(prev_score)
            print(f"Snake: {snake}")
            print(f"Head: {snake_head}")
            print(f"Moves: {moves}")
            #print(f"Socre: {prev_score}")
            print(f"Sum Score: {prev_sum}")

            scores = []
            print(f"Socres for moves: ")
            for move in moves:
                temp_board, temp_snake, temp_head , valid = move_snake(board,snake,snake_head,move)
                if valid:
                    score = cal_scores(temp_board, temp_snake, prev_food)
                    print(score)
                    scores.append(sum_score(score))
                else:
                    scores.append(-1000)
#             print(board)
#             print('\n========')
            if(len(moves) == 0):
                print('no moves')
                print(f"Snake length: {len(snake)}")
                
                print("\n____________________________________\n")
                break
            else:
                bestMove = moves[scores.index(max(scores))]
            print(f"Scores: {scores}")
            print(f"Best Move: {bestMove}")
            print(prev_board)
            print('==========')
            board , snake , snake_head , valid = move_snake(board, snake, snake_head, bestMove)
            score = cal_scores(board, snake, prev_food)
            print(score)
            print(f"{weights}")
            print(f"{snake}")
            print(board)
            
            food = get_food_location(board)
    #         if(len(snake) >=4):
    #             print(weights)

            # except Exception as e:
            #     print(e)
            #     print('game over')
            #     print('snake length -> ',len(snake))
            #     return
    weights = update_weights(weights, score, prev_sum)
    n -= 1
    
main()
